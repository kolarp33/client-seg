from datetime import datetime
from joblib import load
import pandas as pd
import numpy as np
import json

model_filename = 'cust_seg.joblib'
scaler_filename = 'scaler.joblib'

clf = load(model_filename)
scaler = load(scaler_filename)

def preprocess(df):

    df['Date'] = df['Date'].apply(lambda x: pd.Timestamp(x))
    
    #unique ID
    rfm = pd.DataFrame(columns=['Customer_ID','Recency','Frequency','Monetary'])
    rfm['Customer_ID'] = df['Customer ID'].drop_duplicates().sort_values()
    rfm.set_index('Customer_ID',inplace=True)

    #count recency
    recency = df.groupby('Customer ID')['Date'].max()
    recency = np.abs((recency - datetime.now()).dt.days)
    #kedze mame df rfm indexovany podla Customer_ID mozeme priradit takto
    rfm['Recency'] = recency 

    #count freq
    #count je nad stlpcom Date len kvoli triku aby sa mi vratil objekt Series, count je pre vsetky stlpce rovnaky
    frequency = df.groupby('Customer ID')['Date'].count()
    #kedze mame df rfm indexovany podla Customer_ID mozeme priradit takto
    rfm['Frequency'] = frequency

    #count monetary
    monetary = df.groupby('Customer ID')['Subtotal'].sum()
    #kedze mame df rfm indexovany podla Customer_ID mozeme priradit takto
    rfm['Monetary'] = monetary

    return rfm

def scale(df,scaler):
    return scaler.transform(df)

def predict():
    url = 'https://gitlab.fit.cvut.cz/kolarp33/client-seg/raw/master/test.csv'
    df = pd.read_csv(url)
    
    rfm = preprocess(df)
    rfm_scaled = scale(rfm, scaler)

    pred = clf.predict(rfm_scaled)

    clusters = [
        'Superstar',
        'Very good',
        'Good',
        'Poor',
        'Bad'
    ]

    out = lambda x: ' is a ' + clusters[x] + ' customer'
    out_v = np.vectorize(out)
    
    labels = out_v(pred)
    seg = list(zip(rfm.index.values,labels))

    cust_dict =	{}
    

    for i in range(len(seg)):
        cust = 'Customer ID ' + str(seg[i][0])
        cust_dict[cust] = str(seg[i][1])
    #     print('Customer ID ' + str(cust[0]) + str(cust[1]))


    return cust_dict
