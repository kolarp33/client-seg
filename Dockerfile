FROM python:3.6.8
RUN pip install scikit-learn==0.21.3  firefly-python==0.1.15 pandas==0.25.3
COPY app.py scaler.joblib cust_seg.joblib test.csv ./
CMD firefly app.predict --bind 0.0.0.0:$PORT
EXPOSE 5000